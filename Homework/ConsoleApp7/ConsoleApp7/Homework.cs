﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Homework1
{
    public class Homework
    {
        //public string NameHomework;
        public int PointsMax;
        public Dictionary<Student, double> StudentsPoint = new Dictionary<Student, double>();
        //string nameHomework,
        public Homework( int pointsMax)
        {
           // NameHomework = nameHomework;
            PointsMax = pointsMax;
        }

        public static Homework CreateHomework()
        {
            Console.WriteLine("Podaj maksymalną liczbę punktów do uzyskania");
            int maxpoints = Convert.ToInt32(Console.ReadLine());

            return new Homework(maxpoints);
        }

    }
}
