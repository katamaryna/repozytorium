﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Homework1
{
    public class Student
    {

        public string FirstName;
        public string LastName;
        public DateTime Birth;
        public bool Sex;
        public int ID;
        public int Presence = 0;

        public Student(string firstName, string lastName, DateTime birth, bool sex, int iD)
        {
            FirstName = firstName;
            LastName = lastName;
            Birth = birth;
            Sex = sex;
            ID = iD;

        }

        public static Student CreateStudent()
        {
            Console.WriteLine("Podaj imię kursanta");
            string firstName2 = Convert.ToString(Console.ReadLine());

            Console.WriteLine("POdaj nazwisko kursanta");
            string lastName2 = Convert.ToString(Console.ReadLine());

            Console.WriteLine("POdaj datę urdzenia kursanta w formacie yyyy-mm-dd");
            DateTime birth2 = Convert.ToDateTime(Console.ReadLine());

            Console.WriteLine("Podaj płeć kursanta: jeśli to kobieta wpisz true, jeśli mężczyzna wpisz false");
            bool sex2 = Convert.ToBoolean(Console.ReadLine());

            Console.WriteLine("Podaj ID kursanta");
            int iD2 = Convert.ToInt32(Console.ReadLine());

            return new Student(firstName2, lastName2, birth2, sex2, iD2);
        }

    }
}
