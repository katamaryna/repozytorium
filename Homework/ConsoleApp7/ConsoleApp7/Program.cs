﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Homework1
{
    class Program
    {
        private static Journey newCourse;

        static void Main(string[] args)
        {
            Menu.Welcome();

            int option = Convert.ToInt32(Console.ReadLine());
          
            while (option != 5)
            {
                if (option == 1)
                {
                    newCourse = Journey.CreateCourse();
                    Console.WriteLine("Podaj ilu kurasntów wprowadzisz:");
                    int numberOfStudents = Convert.ToInt32(Console.ReadLine());
                    for (int i = 0; i < numberOfStudents; i++)
                    {
                        var student = Student.CreateStudent();
                        newCourse.AddStudent(student);
                    }
                }

                if (option == 2)
                {
                    Console.WriteLine("Wybrano opcję sprawdzenia obecności");

                    {

                        bool itsFinish = false;
                        do
                        {

                            Journey.AddNextDay(newCourse);
                            Console.WriteLine("Dodasz nowy dzień?");
                            itsFinish = Convert.ToBoolean(Console.ReadLine());

                        }
                        while (itsFinish);


                        var helplist = newCourse.ListofStudents;

                        foreach (var item in helplist)
                        {
                            Console.WriteLine($"Czy kursant {item.ID} był obecny? Jeśli tak wpisz 'true' jeśli nie, wpisz 'false'");
                            var wasit = Convert.ToBoolean(Console.ReadLine());
                            if (wasit)
                                item.Presence = 1;

                        }

                        newCourse.AllPresences++;


                     


                    }
                }
                if (option == 3)
                {
                    Console.WriteLine("Wybrano opcję dodawania pracy domowej");
                    
                    var newHomework = Homework.CreateHomework();

                    newCourse.Homework = newHomework;

                    foreach(var singleStudent in newCourse.ListofStudents)

                    {

                        Console.WriteLine($"Punkty uzyskane przez studenta {singleStudent.ID}");
                        var point = Convert.ToInt32(Console.ReadLine());
                        newCourse.Homework.StudentsPoint.Add(singleStudent, point);
                    }


                }
                if (option == 4)
                {
                    Console.WriteLine($"Nazwa kursu: {newCourse.CourseName}");
                    Console.WriteLine($"Data rozpoczęcia kursu: {newCourse.Start}");
                    Console.WriteLine($"Próg obecności do zaliczenia: {newCourse.PresentsMin}");
                    Console.WriteLine($"Próg punktowy do zaliczenia: {newCourse.PassedMin}");
                    Console.WriteLine($"Wyniki obecności studentów:");
                    foreach (var item in newCourse.ListofStudents)

                    {
                        

                        Console.WriteLine($"Student {item.FirstName} {item.LastName} {item.Presence}/{newCourse.AllPresences} ({item.Presence*100/newCourse.AllPresences} %)");

                        if ( item.Presence * 100 / newCourse.AllPresences >  newCourse.PassedMin )
                        {
                            Console.WriteLine("Zaliczone");

                        }
                        else
                        {
                            Console.WriteLine("Niezaliczone");
                        }


                    }

                    Console.WriteLine($"Wyniki z pracy domowej uzyskane przez studentów:");

                    foreach (var item in newCourse.Homework.StudentsPoint)

                    {
                       Console.WriteLine($"Student {item.Key.FirstName} {item.Key.LastName}: {item.Value}/{newCourse.Homework.PointsMax} ({item.Value*100/newCourse.Homework.PointsMax} %)");

                        if (item.Value*100/newCourse.Homework.PointsMax> newCourse.PresentsMin)
                        {

                            Console.WriteLine("Zaliczone");

                        }

                        else

                        {
                            Console.WriteLine("Niezaliczone");
                        }

                    }

               }

                Console.WriteLine("Witamy w dzienniku");
                Console.WriteLine("Wybierz z listy co chcesz zrobić");
                Console.WriteLine("1 - Stwórz nowy dziennik");
                Console.WriteLine("2 - Sprawdź obecność");
                Console.WriteLine("3 -Dodaj pracę domową");
                Console.WriteLine("4 - Wygeneruj raport z kursu");
                Console.WriteLine("5 - Wyjście z progamu");
                option = Convert.ToInt32(Console.ReadLine());
            }

        }
                            
        
        }
    }