﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Homework1
{
    public class Journey
    {
        public string CourseName;
        public string NameofTeacher;
        public DateTime Start;
        public int PassedMin;
        public int PresentsMin;
        public List<Student> ListofStudents = new List<Student>();

        public Journey(string courseName, string nameofTeacher, DateTime start, int passedMin, int presentsMin)
        {
            CourseName = courseName;
            NameofTeacher = nameofTeacher;
            Start = start;
            PassedMin = passedMin;
            PresentsMin = presentsMin;

        }

        public static Journey CreateCourse()

        {
            Console.WriteLine("Podaj nazwę kursu");
            string CourseName2 = Convert.ToString(Console.ReadLine());
            Console.WriteLine("Podaj imię i nazwisko nauczyciela");
            string NameofTeacher2 = Convert.ToString(Console.ReadLine());
            Console.WriteLine("Podaj datę rozpoczęcia kursu w formacie yyyy-mm-dd");
            DateTime Start2 = Convert.ToDateTime(Console.ReadLine());
            Console.WriteLine("Podaj jaki jest próg zaliczenia pracy domowje(w porcentach)");
            int Passedmin2 = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Podaj jaki jes próg zaliczenia obecności (w procentach)");
            int PresentMin2 = Convert.ToInt32(Console.ReadLine());

            return new Journey(CourseName2, NameofTeacher2, Start2, Passedmin2, PresentMin2);


        }

        public int AllPresences = 0;

        public Homework Homework;

        public void  AddStudent (Student student)
        {
            ListofStudents.Add(student);
        }

        public static void AddNextDay(Journey newCourse)
        {
            var helpedlist = newCourse.ListofStudents;

            foreach (var item in helpedlist)

            {
                Console.WriteLine($"Czy kursant {item.ID} był obecny?");
                var wasIt = Convert.ToBoolean(Console.ReadLine());
                if (wasIt)
                    item.Presence = 1;

            }
                      
        }

    }
}
