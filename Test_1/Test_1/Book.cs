﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test_1
{
    public class Book
    {
        public string Title;
        public string AuthorFirstName;
        public string AuthorLastName;
        public int Year;
        public List<Book> ListOfBooks = new  List<Book>();
        public int ID = 0;

        private static int year2;


        public Book(string title, string authorFirstName, string authorLastName, int year)
        {
            Title = title;
            AuthorFirstName = authorFirstName;
            AuthorLastName = authorLastName;
            Year = year;
            

        }

        public void AddBook(Book newBook)
        {
            ListOfBooks.Add(newBook);
        }


        public static Book CreateBook()

        {
            Console.WriteLine("Podaj tytuł książki");
            string title2 = Convert.ToString(Console.ReadLine());
            Console.WriteLine("Podaj imię autora");
            string authorFirstName2 = Convert.ToString(Console.ReadLine());
            Console.WriteLine("Podaj nazwisko autora");
            string authorLastName2 = Convert.ToString(Console.ReadLine());
            Console.WriteLine("Podaj rok wydania książki");
            
            try { 
            int year2 = Convert.ToInt32(Console.ReadLine());
                 }
            catch

            {
                Console.WriteLine("Błędnie wpisałeś rok");
            }

            return new Book(title2, authorFirstName2, authorLastName2, year2);

        }

     

    }
}
