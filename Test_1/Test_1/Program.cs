﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test_1
{
    class Program
    {
        private static Book newBook;
        static void Main(string[] args)
        {
            Menu.Welcome();
            int option = Convert.ToInt32(Console.ReadLine());
            while(option!=4)
            {

                if(option==1)
                {
                    newBook = Book.CreateBook();
                   
                    newBook.AddBook(newBook);
                    newBook.ID++;
               }
               if (option==2)
                {
                    Console.WriteLine("Wybrano opcję szukania książki po tytule");
                    Console.WriteLine("Wpisz tytuł książki");
                    string title = Convert.ToString(Console.ReadLine());
                    if (title == newBook.Title)
                    {
                        Console.WriteLine($"{newBook.ID},{title}, {newBook.AuthorFirstName}, {newBook.AuthorLastName}, {newBook.Year}");
                    }
                    else
                    {
                        Console.WriteLine("Nie ma gakiej pozycji");
                    }
               }

                if (option==3)
                {
                    Console.WriteLine("Wybrano opcję wyszukiwania książki po imieniu i nazwisku autora");
                    Console.WriteLine("Wpisz imię autora");
                    string firstName = Convert.ToString(Console.ReadLine());
                    Console.WriteLine("Wpisz nazwisko autora");
                    string lastname = Convert.ToString(Console.ReadLine());

                    if(firstName==newBook.AuthorFirstName && lastname==newBook.AuthorLastName)
                    {
                        Console.WriteLine($"{newBook.ID},{newBook.Title}, {newBook.AuthorFirstName}, {newBook.AuthorLastName}, {newBook.Year}");
                    }

                    else

                    {
                        Console.WriteLine("Nie ma takiej pozycji");
                    }
                }
               Menu.Welcome();
                option = Convert.ToInt32(Console.ReadLine());
            }

        }

    }
}
